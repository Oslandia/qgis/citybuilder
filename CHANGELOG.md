# CHANGELOG

The format is based on [Keep a Changelog](https://keepachangelog.com/), and this project adheres to [Semantic Versioning](https://semver.org/).

<!--

Unreleased

## version_tag - YYYY-DD-mm

### Added

### Changed

### Removed

-->
## 1.0.0 - 2024-12-02
- Renamed the plugin from CityBuilder to CityForge.
- Added a user case to manage geoflow via Docker or the executable through QGIS settings.
- Introduced unit tests for geoflow execution calls.
- Cleaned up obsolete files.
- Implemented support for English and French translations.
- Updated the documentation.
- Add CI
- Creation of a logo, thank you Sylvain Beorchia for the design

## 0.1.0 - 2024-03-28

- First release
- Generated with the [QGIS Plugins templater](https://oslandia.gitlab.io/qgis/template-qgis-plugin/)
