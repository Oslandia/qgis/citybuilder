#! python3  # noqa E265

import pytest
from qgis.core import QgsPointCloudLayer, QgsVectorLayer

from CityForge.processing.processings import BuildingModelBuilder


@pytest.mark.parametrize(
    "input_path,input_provider,expected_path",
    [
        (
            "/path/to/my/footprint.gpkg|layername=footprint",
            "ogr",
            "/path/to/my/footprint.gpkg",
        ),
        (
            "/path/to/my/footprint.shp",
            "ogr",
            "/path/to/my/footprint.shp",
        ),
        (
            "/path/with some/spaces to/foot print with space in its name.shp",
            "ogr",
            "/path/with some/spaces to/foot print with space in its name.shp",
        ),
        (
            "C:\\Users\\User\\Desktop\\My Documents\\test_data\\footprint.gpkg|layername=footprint",
            "ogr",
            "C:\\Users\\User\\Desktop\\My Documents\\test_data\\footprint.gpkg",
        ),
    ],
)
def test_get_source_path_vector(input_path, input_provider, expected_path):
    o = BuildingModelBuilder()

    layer = QgsVectorLayer(input_path, "vector layer", input_provider)
    assert str(o.get_source_path(layer)) == expected_path


@pytest.mark.parametrize(
    "input_path,input_provider,expected_path",
    [
        (
            "/path/to/my/pointcloud.copc.laz",
            "copc",
            "/path/to/my/pointcloud.copc.laz",
        ),
        (
            "/path/with some/spaces to/point cloud.copc.laz",
            "copc",
            "/path/with some/spaces to/point cloud.copc.laz",
        ),
        (
            "C:\\Users\\User\\Desktop\\My Documents\\test_data\\pointcloud.copc.laz",
            "copc",
            "C:\\Users\\User\\Desktop\\My Documents\\test_data\\pointcloud.copc.laz",
        ),
        (
            "/path/to/my/pointcloud.laz",
            "pdal",
            "/path/to/my/pointcloud.laz",
        ),
        (
            "/path/to/my/pointcloud.las",
            "pdal",
            "/path/to/my/pointcloud.las",
        ),
        (
            "/path/with some/spaces to/point cloud.laz",
            "pdal",
            "/path/with some/spaces to/point cloud.laz",
        ),
        (
            "C:\\Users\\User\\Desktop\\My Documents\\test_data\\pointcloud.laz",
            "pdal",
            "C:\\Users\\User\\Desktop\\My Documents\\test_data\\pointcloud.laz",
        ),
    ],
)
def test_get_source_path_pointcloud(input_path, input_provider, expected_path):
    o = BuildingModelBuilder()

    layer = QgsPointCloudLayer(input_path, "point cloud layer", input_provider)
    assert str(o.get_source_path(layer)) == expected_path
