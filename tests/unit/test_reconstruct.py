#! python3  # noqa E265

import subprocess
import unittest
from pathlib import Path
from unittest.mock import Mock, patch

from CityForge import reconstruct


class TestJsonConstructor(unittest.TestCase):
    """
    Test the reconstruct method for calls the geoflow command.

    The test mocks: os.path.isdir, PlgOptionsManager, subprocess.run

    The test ensures that:
    - The input parameters.
    - The subprocess.run arguments.
    - The command output matches the expected result.

    """

    @patch("subprocess.run")
    @patch("os.path.isdir")
    def test_reconstruct(self, isdir_mock, mock_subprocess_run):

        # Mock settings for PlgOptionsManager
        get_plg_settings_mock = Mock()
        get_plg_settings_mock.geoflow_install = "docker"
        get_plg_settings_mock.docker_path = "docker"
        get_plg_settings_mock.executable_path = (
            "C:\\Program Files\\Geoflow\\bin\\geoflow.exe"
        )

        # Mock the os.path.isdir return value
        isdir_mock.return_value = True

        # Define the CityForge directory path
        cityforge_directory = (
            Path(__file__).absolute().parent.parent.parent / "CityForge"
        )

        """
        Mock the docker command
        """

        # Define the expected docker command
        expected_command_docker = [
            "docker",
            "run",
            "--rm",
            "--network=host",
            "-v",
            "/path/to:/fp",
            "-v",
            "/path/to:/pc",
            "-v",
            "/path/to/output:/out",
            "ignfab/lod22-reconstruct-batch:latest",
            "--input_footprint=/fp/footprint",
            "--input_pointcloud=/pc/pointcloud",
            "--output_cityjson=/out/model.json",
            "--output_cj_referenceSystem=EPSG::2154",
        ]

        # Mock the subprocess.run return value

        expected_cmd = mock_subprocess_run.return_value = subprocess.CompletedProcess(
            args=expected_command_docker, returncode=0
        )

        # Call the method under test with mock inputs
        o = reconstruct.JsonConstructor()
        cmd = o.reconstruct(
            Path("/path/to/footprint"),
            Path("/path/to/pointcloud"),
            Path("/path/to/output"),
            None,
            "2154",
            get_plg_settings_mock.geoflow_install,
            get_plg_settings_mock.docker_path,
            get_plg_settings_mock.executable_path,
        )
        self.assertEqual(expected_cmd, cmd)

        """
        Mock the executable command
        """

        get_plg_settings_mock.geoflow_install = "executable"

        # Define the expected executable command

        expected_command_exe = [
            f'"C:\\Program Files\\Geoflow\\bin\\geof.exe" {cityforge_directory}\\flowchart\\reconstruct.json'
            " --input_footprint=/path/to/footprint",
            " --input_pointcloud=/path/to/pointcloud",
            " --output_cityjson=/path/to/output\\model.json",
            " --output_cj_referenceSystem=EPSG::2154",
        ]
        expected_cmd = mock_subprocess_run.return_value = subprocess.CompletedProcess(
            args=expected_command_exe, returncode=0
        )

        cmd = o.reconstruct(
            Path("/path/to/footprint"),
            Path("/path/to/pointcloud"),
            Path("/path/to/output"),
            None,
            "2154",
            get_plg_settings_mock.geoflow_install,
            get_plg_settings_mock.docker_path,
            get_plg_settings_mock.executable_path,
        )

        self.assertEqual(cmd, expected_cmd)
