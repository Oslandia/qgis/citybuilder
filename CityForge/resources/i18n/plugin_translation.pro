FORMS = ../../gui/dlg_settings.ui

SOURCES=  ../../plugin_main.py \
    ../../gui/dlg_settings.py \
    ../../toolbelt/log_handler.py \
    ../../toolbelt/preferences.py \
    ../../processing/processings.py \
    ../../processing/provider.py \
    ../../reconstruct.py \

TRANSLATIONS = cityforge_fr.ts
