# CityForge - QGIS Plugin

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![flake8](https://img.shields.io/badge/linter-flake8-green)](https://flake8.pycqa.org/)

![Logo](./CityForge/resources/images/icon.png)

## Description

This plugin reconstruct 3D buildings from footprint and point cloud.

### Documentation

The documentation is generated using Sphinx and is automatically generated through the CI and published on Pages.

- documentation and homepage: <https://oslandia.gitlab.io/qgis/cityforge>
- repository: <https://gitlab.com/Oslandia/qgis/cityforge/>
- tracker: <https://gitlab.com/Oslandia/qgis/cityforge//-/issues>

----

## License

Distributed under the terms of the [`GPLv2+` license](LICENSE).
