<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr" sourcelanguage="en">
<context>
    <name>BuilderPlugin</name>
    <message>
        <location filename="../../plugin_main.py" line="73"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="82"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
</context>
<context>
    <name>BuilderProvider</name>
    <message>
        <location filename="../../processing/provider.py" line="35"/>
        <source>CityForge</source>
        <translation>CityForge</translation>
    </message>
    <message>
        <location filename="../../processing/provider.py" line="45"/>
        <source>Building model builder - Tools</source>
        <translation>Constructeur de modèles de bâtiments - Outils</translation>
    </message>
</context>
<context>
    <name>BuildingModelBuilder</name>
    <message>
        <location filename="../../processing/processings.py" line="55"/>
        <source>Generate CityJSON file</source>
        <translation>Générer un fichier CityJSON</translation>
    </message>
    <message>
        <location filename="../../processing/processings.py" line="62"/>
        <source>CityForge</source>
        <translation>CityForge</translation>
    </message>
    <message>
        <location filename="../../processing/processings.py" line="86"/>
        <source>Select PointCloud file las or laz</source>
        <translation>Sélectionnez le fichier nuage de point las ou laz</translation>
    </message>
    <message>
        <location filename="../../processing/processings.py" line="93"/>
        <source>Select footprint input file</source>
        <translation>Sélectionner le fichier d'entrée de l'emprise</translation>
    </message>
    <message>
        <location filename="../../processing/processings.py" line="106"/>
        <source>Generate only LOD 2.2</source>
        <translation>Générer seulement LOD 2.2</translation>
    </message>
    <message>
        <location filename="../../processing/processings.py" line="71"/>
        <source>Uses geoflow for generating a CityJSON file.
Takes the point cloud and the 2D polygon of one or multiple buildings.</source>
        <translation>Utilise geoflow pour générer le fichier CityJSON
Prend un nuage de point et un polygon 2D comprenant un ou plusieurs bâtiments.</translation>
    </message>
    <message>
        <location filename="../../processing/processings.py" line="100"/>
        <source>Output directory</source>
        <translation>Répertoire de sortie</translation>
    </message>
    <message>
        <location filename="../../processing/processings.py" line="213"/>
        <source>CRS ERROR: could not find the CRS of the footprint layer, the input format may not be supported</source>
        <translation>ERREUR CRS: impossible de trouver le CRS de la couche d'emprise, le format d'entrée n'est peut-être pas supporté</translation>
    </message>
    <message>
        <location filename="../../processing/processings.py" line="222"/>
        <source>CRS ERROR: could not find the CRS of the pointcloud layer, the input format may not be supported</source>
        <translation>ERREUR CRS: impossible de trouver le CRS de la couche de nuage de points, le format d'entrée n'est peut-être pas supporté</translation>
    </message>
    <message>
        <location filename="../../processing/processings.py" line="231"/>
        <source>CRS ERROR: the CRS of the footprint layer &apos;{}&apos; and the pointcloud layer &apos;{}&apos; don&apos;t match</source>
        <translation>ERREUR CRS: le CRS de la couche d'emprise '{}' et de la couche de nuage de point '{}' ne correspondent pas</translation>
    </message>
    <message>
        <location filename="../../processing/processings.py" line="193"/>
        <source>Check your CityForge parameters regarding how you installed Geoflow</source>
        <translation>Vérifiez dans vos paramètres comment vous installez Geoflow</translation>
    </message>
    <message>
        <location filename="../../processing/processings.py" line="141"/>
        <source>PointCloud ERROR: The point cloud is not classified. It should contain at least buildings and ground.</source>
        <translation>ERREUR Nuage de point: Le nuage de point n'est pas classifié. La classification devrait au moins contenir les bâtiments et le sol.</translation>
    </message>
    <message>
        <location filename="../../processing/processings.py" line="150"/>
        <source>Running reconstruction, this can take a while...</source>
        <translation>Reconstruction en cours, ceci peut prendre du temps...</translation>
    </message>
    <message>
        <location filename="../../processing/processings.py" line="164"/>
        <source>Geoflow output:
{}</source>
        <translation>Sortie Geoflow:
{}</translation>
    </message>
    <message>
        <location filename="../../processing/processings.py" line="168"/>
        <source>Geoflow warnings:
{}</source>
        <translation>Avertissements Geoflow:
{}</translation>
    </message>
    <message>
        <location filename="../../processing/processings.py" line="173"/>
        <source>Geoflow exited with non-zero code {}</source>
        <translation>Geoflow a terminé avec code de sortie non null {}</translation>
    </message>
    <message>
        <location filename="../../processing/processings.py" line="180"/>
        <source>Checking file exists at {}</source>
        <translation>Vérification de l'existence du fichier à {}</translation>
    </message>
    <message>
        <location filename="../../processing/processings.py" line="182"/>
        <source>Geoflow did not output any data, or the file is not readable</source>
        <translation>Geoflow n'a pas généré de fichier, ou le fichier n'est pas lisible</translation>
    </message>
    <message>
        <location filename="../../processing/processings.py" line="190"/>
        <source>Success, file saved at: {}</source>
        <translation>Succès, fichier généré à : {}</translation>
    </message>
</context>
<context>
    <name>ConfigOptionsPage</name>
    <message>
        <location filename="../../gui/dlg_settings.py" line="250"/>
        <source>Geoflow settings</source>
        <translation>Paramètres Geoflow</translation>
    </message>
</context>
<context>
    <name>wdg_cityforge_settings</name>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="44"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;PluginTitle - Version X.X.X&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&amp;lt;html&amp;gt;&amp;lt;head/&amp;gt;&amp;lt;body&amp;gt;&amp;lt;p align=« center »&amp;gt;&amp;lt;span style=«  font-weight:600 ; »&amp;gt;Titre du plugin - Version X.X.X&amp;lt;/span&amp;gt;&amp;lt;/p&amp;gt;&amp;lt;/body&amp;gt;&amp;lt;/html&amp;gt;</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="180"/>
        <source>Miscellaneous</source>
        <translation>Divers</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="226"/>
        <source>Report an issue</source>
        <translation>Signaler un problème</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="204"/>
        <source>Version used to save settings:</source>
        <translation>Version utilisée pour sauvegarder les paramètres:</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="279"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="245"/>
        <source>Enable debug mode.</source>
        <translation>Activer le mode débogage.</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="254"/>
        <source>Debug mode (degraded performances)</source>
        <translation>Mode débug (performances dégradées)</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="66"/>
        <source>Geoflow installation</source>
        <translation>Installation de geoflow</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="79"/>
        <source>Help to choose :</source>
        <translation>Aide pour choisir</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="72"/>
        <source>geof path:</source>
        <translation>Chemin vers geof:</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="86"/>
        <source>Docker image</source>
        <translation>Image Docker</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="93"/>
        <source>Test installation</source>
        <translation>Tester l'installation</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="100"/>
        <source>docker path:</source>
        <translation>Chemin vers docker:</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="107"/>
        <source>Executable</source>
        <translation>Exécutable</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="119"/>
        <source>You must install geof.exe before</source>
        <translation>Vous devez d'abord installer geof.exe</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="150"/>
        <source>You must build the images before</source>
        <translation>Vous devez d'abord construire les images</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="14"/>
        <source>CityForge - Settings</source>
        <translation>CityForge - Paramètres</translation>
    </message>
</context>
</TS>
