import processing
from qgis.core import QgsProcessingAlgorithm


def loader_layer(
    self,
    json_file,
    crs,
    feedback: QgsProcessingAlgorithm.feedback,
    context: QgsProcessingAlgorithm.context,
):

    loaded = processing.run(
        "cityjsonloader:loadcityjson",
        {
            "INPUT": str(json_file),
            "DIVIDE_BY_OBJECT_TYPE": False,
            "LOD_AS": 0,
            "LOAD_SEMANTIC_SURFACES": False,
            "STYLE_BY_SEMANTIC_SURFACES": False,
            "SRID": crs,
            "BBOX": None,
            "OBJECT_TYPE": [],
        },
        context=context,
        feedback=feedback,
    )
