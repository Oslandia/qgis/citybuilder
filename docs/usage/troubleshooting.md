# Troubleshooting

## LAS and LAZ files cannot be opened by this QGIS install

Loading LAS/LAZ files in QGIS require PDAL, which may not be available. While QGIS can still load COPC LAZ files, Geoflow requires LAS/LAZ file and does not handle COPC files. You will need to install a QGIS version that includes PDAL.

For instance, you can:

1. Install the Flatpak version of QGIS from <https://qgis.org/resources/installation-guide/#flatpak>
2. Give permissions to access Docker from QGIS: `flatpak override --user --filesystem=/run/docker.sock org.qgis.qgis`
3. In the plugin settings, modify the path to docker, from `docker` to `/run/host/bin/docker`


![QGIS_PDAL](pictures/QGIS_PDAL.png)

## Docker rootless

You may encounter writing permission issues when launching the QGIS plugin. This could be related to [permission problems with Docker](https://docs.docker.com/engine/security/rootless/#install). In this case, follow these steps:

1. Run `sudo systemctl disable --now docker.service docker.socket`
2. Reboot your computer
3. Run `sudo apt-get install -y uidmap`
4. Run `dockerd-rootless-setuptool.sh install`

## Attribute Definitions

```shell
ERROR: Error: Number of attributes not equal to number of geometries [field name =OGRLoader.aantal_verblijfsobjecten]
```

Try in root user.

## CityJSon Loader : error message

``` AttributeError: 'QgsPolygon3DSymbol' object has no attribute 'setMaterial'```

Note that for QGIS 3.30 and later, there is currently a bug preventing from loading files, but can be patched (see <https://github.com/cityjson/cityjson-qgis-plugin/issues/57>).
