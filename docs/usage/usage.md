# Using CityForge

## JSON generation

From the processing toolbox (which may not be visible by default), click on CityForge:

- add the shapefile which will be used as footprint
- add the point cloud in .las format
- Choose the location where you want to save your JSON file

![Launch CityForge processing](pictures/cityForge_loader.png)

## Visualization of the JSON with the CityJSON Loader plugin

From the processing toolbox click on CityJSON Loader, or from the CityJSON toolbar:

- add the JSON file you have previously generated
- check the box Split Layers according to object type
- check the box Load semantic surfaces (so that you have all the Lod differentiated in the display of your building)

![Load generated CityJSON file](pictures/cityJson_loader.png)

You need to configure the 3D styles in the QGIS properties by going to **Properties > 3D View**.

![Style configuration](pictures/style_properties.png)

Then go to the View tab > 3D View > New 3D Map View. After adding the Digital Terrain Model (DTM), you will be able to see your reconstructed building in 3D.

![3D_DMT](pictures/3D_configuration.png)

![3D_View](pictures/view_3D.png)
