# {{ title }} - Documentation

> **Description:** {{ description }}  
> **Author and contributors:** {{ author }}  
> **Plugin version:** {{ version }}  
> **QGIS minimum version:** {{ qgis_version_min }}  
> **QGIS maximum version:** {{ qgis_version_max }}  
> **Source code:** {{ repo_url }}  
> **Last documentation update:** {{ date_update }}

----

This QGIS plugin allows the reconstruction and visualization in 3D of buildings in QGIS. The plugin uses several softwares:

- Geoflow which is the backend of this plugin  
    Geoflow is a software developed by Dutch researchers that takes as input two main data sources: the BAG data (BAG being the most detailed building and address register available in open source in the Netherlands), and the AHN data (digital elevation map of the Netherlands). From these two data sources, geowflow will reconstruct these buildings in 3D, after which these buildings will be visualized in 3D Bag which is a 3D viewer and which provides a 3D model of the Netherlands.
- CityJSON Loader which is a QGIS plugin to load CityJSON datasets.

Downloading these two modules is necessary in order to fully use the CityForge plugin - steps to install them are described in the Installation section.

## Types of data taken in by the CityForge plugin

This plugin takes two types of data as input, with the same requirements as [Geoflow](https://github.com/geoflow3d/geoflow-bundle#requirements-on-the-input-data):

- Footprint of buildings:
  - Preferably roofprint,
  - In shapefile (shp) or Geopackage (gpkg) format,
- Classified point cloud:
  - Acquired through aerial scanning, either Lidar or Dense Image Matching,
  - Classified, with at least ground and building classes.
  - In .las or .laz format

If your point cloud is not classified, there is a quick and dirty utility script provided in the plugin to classify it based on the footprint layer at `classify.py`.

Below are links to the site where you can get these datasets:

- For the French cadastral data: <https://cadastre.data.gouv.fr/data/etalab-cadastre/latest/shp/>
- For Lidar data: <https://geoservices.ign.fr/lidarhd>

----

```{toctree}
---
caption: Usage
maxdepth: 1
---
Installation <usage/installation>
Test data <usage/test_data>
Usage <usage/usage>
Troubleshooting <usage/troubleshooting>
```

```{toctree}
---
caption: Contribution guide
maxdepth: 1
---
development/contribute
development/environment
development/documentation
development/translation
development/packaging
development/testing
development/history
```
